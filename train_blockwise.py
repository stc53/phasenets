
'''
Copyright 2017 TensorFlow Authors and Sheldon Coup

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam, Adadelta
import inception_v4
import numpy as np
import cv2
import os
import pickle
import shutil
import gc
from keras.utils.training_utils import multi_gpu_model
import time
from plotting_blockwise import plot_ete, plot_phase

# If you want to use a GPU set its index here
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

#### DATASET PARAMETERS ####
# Name of the dataset to train on
dataset_to_use = 'caltech-birds'
# Number of classes in the dataset
num_classes = 200
# Where the images can be found
data_dir = '/Scratch/stc53/images/'
#If the images need to be sorted into train/valid/test directories, what file is to be used for that
split_file_dir = '/Scratch/stc53/caltech_birds_trainsize_20_validation.p'
#If the images need to be sorted into train/valid/test directories, where should they be moved to 
target_data_dir = '/Scratch/stc53/images-split/'

#### MODEL PARAMETERS ####
#Which model to use
model_name = 'inceptionV4'
#How many training epochs to do between validation set checking
epoch_per_run = 1
# if the validation accuracy decreases by less than this amount in a run, 
#the script considers the program to have stalled for that run
accuracy_metric = 0.0025
# batch size
batch_size = 32
# how many sections the given model has been split into
# Do not change unless the model model structure has been specifically tweaked
total_phases = 5
#where the checkpoints and info pickle files should be saved to
checkpoint_dir = '/localhome/stc53/phasenets-saves/checkpoints/'
# how many times the training has to have been considered to be stalled before the training is cancelled
stall_cutoff = 25
# Boolean to check if the script should load the imagenet weights checkpoint
load_imagenet = False
# minimum number of epochs to train for and how often to save in this training stage
min_epoch = 200



#### OPTIMIZER PARAMETERS ####
#What optimizer to use
opt_name = 'Adadelta'
#Learning rate
lr = None


#### MISC OPTIONS ####
#Whether or not the data needs to be moved into train/test/validation directories
data_to_be_moved = False
# Whether or not the moved data should be deleted at the end of training
# Non functional currenty, can be implemented to enable automatic training on a number of of different training set sizes
clear_moved_data = False
#whether or not the n-1 phase layers should be frozen when training a model of phase n
# Set True for phase training, False for Complete (end to end) training 
freeze_layers=True
# boolean to decide if exit conditions should be used
exit_if_stalled = True
# seed to use for random initializations
rand_seed = 1
#whether or not to use multi-gpu training
multigpu = False






def copy_file_list(file_list, target_data_dir):
  # Given a list of file directories, moves them into the given target directory 
  if not os.path.exists(target_data_dir):
    os.mkdir(target_data_dir)
  
  for img_dir in file_list:
    classname = os.path.basename(os.path.dirname(img_dir))
    img_name = os.path.basename(img_dir)
    new_img_dir = os.path.join(target_data_dir, classname)
    if not os.path.exists(new_img_dir):
      os.mkdir(new_img_dir)
    shutil.copy(img_dir, new_img_dir)
    
def move_data(split_file_dir, target_data_dir ):
  #moves data into directories according to the given split file
  print('Moving images around to simplify data pipeline')
  with open(split_file_dir, 'rb') as f:
    split_dict = pickle.load(f)
  
  print('Shuffling training images')
  X_train = split_dict['X_train']
  y_train = split_dict['y_train']
  
  train_dir = os.path.join(target_data_dir, 'train')
  copy_file_list(X_train, train_dir)
  
  
  print('Shuffling test images')
  X_test = split_dict['X_test']
  y_test = split_dict['y_test']
  
  test_dir = os.path.join(target_data_dir, 'test')
  copy_file_list(X_test, test_dir)
  
  if 'X_valid' in split_dict.keys():
    print('Shuffling validation images')
    X_valid = split_dict['X_valid']
    y_valid = split_dict['y_valid']
  
    valid_dir = os.path.join(target_data_dir, 'valid')
    copy_file_list(X_valid, valid_dir)
    
  print('Image moving complete')
    
    
def model_loader(model_name, num_classes = 200, weights=None, include_top=True, phase=4, freeze_layers = False):
  # wrapper for loading models, currently only for the inceptionV4 model but more to be created in the future.
  if model_name == 'inceptionV4':
    return inception_v4.create_model(num_classes = num_classes, weights=weights, include_top=True, phase=phase, freeze_layers=freeze_layers)

def optimizer_loader(optimizer_name, learning_rate = None, decay_rate = 0.0):
  if learning_rate == None:
    if optimizer_name == 'Adadelta':
      return Adadelta()
    elif optimizer_name == 'Adam':
      return Adam()
  else:
    if optimizer_name == 'Adadelta':
      return Adadelta(lr = learning_rate, decay_rate = decay_rate)
    elif optimizer_name == 'Adam':
      return Adam(lr = learning_rate, decay_rate = decay_rate)
    
  

if __name__ == "__main__":
  if data_to_be_moved:
    move_data(split_file_dir, target_data_dir)
  
  # initialize the data pipelines
  train_datagen = ImageDataGenerator(
                                    rescale = 1./255,
                                    shear_range = 0.2,
                                    zoom_range = 0.2,
                                    horizontal_flip = True,
                                    rotation_range = 20)
  
  test_datagen = ImageDataGenerator(rescale=1./255)
  
  train_generator = train_datagen.flow_from_directory(
                                    os.path.join(target_data_dir, 'train'),
                                    target_size = (299,299),
                                    batch_size = batch_size,
                                    seed = rand_seed)
  
  test_generator = test_datagen.flow_from_directory(
                                    os.path.join(target_data_dir, 'test'),
                                    target_size = (299,299),
                                    batch_size = batch_size,
                                    seed = rand_seed)
  
  valid_generator = test_datagen.flow_from_directory(
                                    os.path.join(target_data_dir, 'valid'),
                                    target_size = (299,299),
                                    batch_size = batch_size,
                                    seed = rand_seed)
  # inital setup
  if load_imagenet:
    weights_to_load = 'imagenet'
  else:
    weights_to_load = 'random'
  model = None                                 
  init_phase = 4 if not freeze_layers else 0
  for phase_num in range(init_phase,total_phases):
    
    # clear previous model from memory to prevent memory leaking
    if model:
        print('Clearing model from memory')
        del model
        for b in range(10):
            gc.collect()
        K.clear_session()

    
    # Create model and load pre-trained weights
    model = model_loader(model_name, num_classes = num_classes, weights=weights_to_load, include_top=True, phase=phase_num, freeze_layers = freeze_layers)
    if multigpu:
      model = multi_gpu_model(model, gpus=4)
    opt = optimizer_loader(optimizer_name, learning_rate = lr) 
    model.compile(loss= 'categorical_crossentropy',optimizer=opt, metrics=['accuracy'])
    model.summary()

    #Setup variables to track progress of training
    still_learning = True
    training_stalled = 0
    cycle_count = 0
    top_valid_acc = 0.
    prev_valid_acc = 0.
    start_time = time.time()
    if freeze_layers:
      run_name =  model_name + '_phase_' + str(phase_num)
    else:
      run_name = model_name + '_end_to_end' 
    checkpoint_name = os.path.join( checkpoint_dir, run_name + '.h5')
    log_dir = os.path.join(checkpoint_dir, run_name +'.p') 
    train_record = {'run_name':run_name, 
                    'train_acc':[],
                    'train_loss':[], 
                    'valid_acc':[], 
                    'valid_loss':[], 
                    'start_time':time.time()}

    while still_learning:
      #Train the model on the training data for a set number of epochs
      run_hist = model.fit_generator(train_generator, validation_data = valid_generator, initial_epoch=cycle_count*epoch_per_run, epochs = (cycle_count + 1) * epoch_per_run)
      #check how well the model if doing on validation data
      current_valid_loss = run_hist.history['val_loss'][-1]
      current_valid_acc = run_hist.history['val_acc'][-1]
      
      
      if current_valid_acc >= top_valid_acc:
        #if current validation accuracy is the best so far, then save the model
        model.save_weights( checkpoint_name)
        top_valid_acc = current_valid_acc
        training_stalled = 0
      if (current_valid_acc <= prev_valid_acc or abs(current_valid_acc - prev_valid_acc) < accuracy_metric) and cycle_count > min_epoch:
        # check if accuracy has increased since the last run, if not increase training_stalled count,
        training_stalled += 1

      # print information so training can be monitored
      print('Current Validation Accuracy : ' + str(current_valid_acc))
      print('Current Validation Loss : ' + str(current_valid_loss))
      print('Top Validation Accuracy : ' + str(top_valid_acc))
      if exit_if_stalled:
        print('Stall count : ' + str(training_stalled))
      
      # record information about run to be saved later
      train_record['train_acc'].extend(list(run_hist.history['acc']))
      train_record['train_loss'].extend(list(run_hist.history['loss']))
      train_record['valid_acc'].extend(list(run_hist.history['val_acc']))
      train_record['valid_loss'].extend(list(run_hist.history['val_loss']))
      
      # dump the current train record in a pickle file and plot the results
      with open(log_dir, 'wb') as f:
        pickle.dump(train_record,f)
      if freeze_layers:
        plot_phase(phase_num, model_name, log_dir, total_phases)
      else:
        plot_ete(model_name, log_dir)
      
      #check if its time to stop training    
      if training_stalled > stall_cutoff and exit_if_stalled :
        still_learning = False
      
      #update tracking variables
      prev_valid_acc = current_valid_acc
      cycle_count += 1
      
    
    
    #final recording and writing of tracking variables  
    train_record['end_time'] = time.time()
    test_loss, test_acc = model.evaluate_generator(test_generator)
    train_record['test_acc'] = test_acc
    train_record['test_loss'] = test_loss
    with open(log_dir, 'wb') as f:
      pickle.dump(train_record,f)
    
    weights_to_load = checkpoint_name
    #model.save_weights(weights_to_load)
  test_loss, test_acc = model.evaluate_generator(test_generator)

  print()
  print('Test loss : ' + str(test_loss))
  print('Test acc : ' + str(test_acc))
