import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
import pickle
import os
log_dir = '/localhome/stc53/phasenets/keras-inceptionV4/checkpoints/'
model_name = 'inceptionV4'
num_phases = 5
plot_both = True
plot_phase_do = True
plot_ete_do = False


color=iter(cm.rainbow(np.linspace(0,1,num_phases)))
def plot_phase(phase_num, model_name, log_dir, num_phases):
  color = iter(cm.rainbow(np.linspace(0,1,num_phases)))
  fig = plt.figure(figsize=(20,20))
  ax = fig.add_subplot(111)
  for i in range(phase_num + 1):
    file_dir = os.path.join(log_dir, model_name + '_phase_' + str(i)+ '.p')
    with open(file_dir, 'rb') as f:
      run_dict = pickle.load(f)
    num_epochs = len(run_dict['train_acc'])
    c = next(color)
    ax.plot(range(num_epochs), run_dict['train_acc'], '-',c=c, label='phase ' + str(i) + ' train')
    ax.plot( range(len(run_dict['valid_acc'])),run_dict['valid_acc'], '--',c=c ,label='phase ' + str(i) + ' valid')
    plt.autoscale(tight=True)
    plt.legend()
    
  fig.savefig(model_name + ' acc.png')
  color=iter(cm.rainbow(np.linspace(0,1,num_phases)))
  
  fig = plt.figure(figsize=(20,20))
  ax = fig.add_subplot(111)
  
  for i in range(num_phases):
    file_dir = os.path.join(log_dir, model_name + '_phase_' + str(i)+ '.p')
    with open(file_dir, 'rb') as f:
      run_dict = pickle.load(f)
    num_epochs = len(run_dict['train_loss'])
    c = next(color)
    ax.plot(range(num_epochs), run_dict['train_loss'], '-',c=c, label='phase ' + str(i) + ' train')
    ax.plot(range(len(run_dict['valid_loss'])),run_dict['valid_loss'], '--', c=c,label='phase ' + str(i) + ' valid')
    plt.autoscale(tight=True)
    plt.legend()
  
  
  fig.savefig(model_name + 'loss.png')
  plt.close('all')

def plot_ete(model_name, log_dir):
  color = iter(cm.rainbow(np.linspace(0,1, 1)))
  fig = plt.figure(figsize=(20,20))
  ax = fig.add_subplot(111)
  file_dir = os.path.join(log_dir, model_name + '_end_to_end.p')
  with open(file_dir, 'rb') as f:
    run_dict = pickle.load(f)
  num_epochs = len(run_dict['train_acc'])
  c = next(color)
  ax.plot(range(num_epochs), run_dict['train_acc'], '-',c=c, label='end to end train')
  ax.plot( range(len(run_dict['valid_acc'])),run_dict['valid_acc'], '-',c=c ,label='end to end valid')
  plt.autoscale(tight=True)
  plt.legend()
    
  fig.savefig(model_name + ' acc_ete.png')
  color=iter(cm.rainbow(np.linspace(0,1,num_phases)))
  
  fig = plt.figure(figsize=(20,20))
  ax = fig.add_subplot(111)
  

  file_dir = os.path.join(log_dir, model_name + '_end_to_end.p')
  with open(file_dir, 'rb') as f:
    run_dict = pickle.load(f)
  num_epochs = len(run_dict['train_loss'])
  c = next(color)
  ax.plot(range(num_epochs), run_dict['train_loss'], '-',c=c, label='end to end train')
  ax.plot( range(len(run_dict['valid_loss'])),run_dict['valid_loss'], '-', c=next(color),label='end to end valid')
  plt.autoscale(tight=True)
  plt.legend()
  
  
  fig.savefig(model_name + 'loss_ete.png')
  plt.close('all')

def plot_both(phase_num, model_name, log_dir, num_phases):
  color = iter(cm.rainbow(np.linspace(0,1,num_phases+1)))
  fig = plt.figure(figsize=(20,20))
  ax = fig.add_subplot(111)
  for i in range(phase_num + 1):
    file_dir = os.path.join(log_dir, model_name + '_phase_' + str(i)+ '.p')
    with open(file_dir, 'rb') as f:
      run_dict = pickle.load(f)
    num_epochs = len(run_dict['train_acc'])
    c = next(color)
    ax.plot(range(num_epochs), run_dict['train_acc'], '-',c=c, label='phase ' + str(i) + ' train')
    ax.plot( range(len(run_dict['valid_acc'])),run_dict['valid_acc'], '--',c=c ,label='phase ' + str(i) + ' valid')
  
  file_dir = os.path.join(log_dir, model_name + '_end_to_end.p')
  with open(file_dir, 'rb') as f:
    run_dict = pickle.load(f)
  num_epochs = len(run_dict['train_acc'])
  c = next(color)
  ax.plot(range(num_epochs), run_dict['train_acc'], '-',c=c, label='end to end train')
  ax.plot( range(len(run_dict['valid_acc'])),run_dict['valid_acc'], '--',c=c ,label='end to end valid')
  
  plt.autoscale(tight=True)
  plt.legend()
    
  fig.savefig(model_name + ' acc.png')
  
  color=iter(cm.rainbow(np.linspace(0,1,num_phases+1)))
  
  fig = plt.figure(figsize=(20,20))
  ax = fig.add_subplot(111)
  
  for i in range(num_phases):
    file_dir = os.path.join(log_dir, model_name + '_phase_' + str(i)+ '.p')
    with open(file_dir, 'rb') as f:
      run_dict = pickle.load(f)
    num_epochs = len(run_dict['train_loss'])
    c = next(color)
    ax.plot(range(num_epochs), run_dict['train_loss'], '-',c=c, label='phase ' + str(i) + ' train')
    ax.plot(range(len(run_dict['valid_loss'])),run_dict['valid_loss'], '--', c=c,label='phase ' + str(i) + ' valid')
  
  file_dir = os.path.join(log_dir, model_name + '_end_to_end.p')
  with open(file_dir, 'rb') as f:
    run_dict = pickle.load(f)
  num_epochs = len(run_dict['train_loss'])
  c = next(color)
  ax.plot(range(num_epochs), run_dict['train_loss'], '-',c=c, label='end to end train')
  ax.plot( range(len(run_dict['valid_loss'])),run_dict['valid_loss'], '--', c=c,label='end to end valid')
  
  plt.autoscale(tight=True)
  plt.legend()
  
  
  fig.savefig(model_name + 'loss.png')
  plt.close('all')

if __name__ == '__main__':
  if plot_both:
    plot_both(num_phases-1, model_name, log_dir, num_phases)
  else:
    if plot_ete_do:
      plot_ete(model_name, log_dir)
    elif plot_phase_do:
      plot_phase(num_phases-1, model_name, log_dir, num_phases)
  
  
